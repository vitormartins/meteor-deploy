# meteor-deploy
Meteor Deploy from Git Repository

### Configuration ###
Open the script in your favorite editor and set first lines.
```
USER="my_user"
FULLPATH="/home/$USER/my_app"
GITREPOSITORY="git@bitbucket.org:owner/repository.git"
GITBRANCH="master"
GITSSHKEY="$FULLPATH/ssh/bitbucket"
SERVERNAME="my-app.com"
PORT="8080"
INITSYSTEM="systemd"
```

### Install ###
```
$ chmod +x meteor-deploy.sh
```

### Usage: ###
Run on server:
```
$ sudo meteor-deploy.sh
```