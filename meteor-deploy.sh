#!/bin/bash

##################################################################
# meteor-deploy.sh                                               #
# Script para realizar o deploy da aplicação meteor no servidor. #
# Autor: Vitor Martins                                           #
##################################################################

# Parâmetros de configuração
USER="my_user" #nome do usuário
FULLPATH="/home/$USER/my_app" #local de instalação do Meteor (sem barra no final)
GITREPOSITORY="git@bitbucket.org:owner/repository.git" #url do repositório git
GITBRANCH="master" #nome do branch no git
GITSSHKEY="$FULLPATH/ssh/bitbucket" #chave SSH utilizada para acessar o repositório GIT (gerar chave: ssh-keygen -t rsa)
SERVERNAME="my-app.com" #nome do servidor (sem protocolo)
PORT="8080" #porta interna que será executado o meteor
INITSYSTEM="systemd" # upstart ou systemd
# --

NODE=$(which node)
METEOR=$(which meteor)
GIT=$(which git)
MONGO=$(which mongo)
NGINX=$(which nginx)
NPM=$(which npm)

configure_nginx() {
    cat > "/etc/nginx/sites-enabled/$SERVERNAME.conf" << EOF
map \$http_upgrade \$connection_upgrade {
    default upgrade;
    ''      close;
}
server {
    listen 80;
    server_name $SERVERNAME;

    root /usr/share/nginx/html; #irrelevante

    # Encaminha todas as requisições para o Meteor
    location / {
        proxy_pass http://127.0.0.1:$PORT;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection \$connection_upgrade;
        proxy_set_header X-Forwarded-For \$remote_addr;

        if ($uri != '/') {
            expires 30d;
        }
    }
}
EOF

    if [ "$INITSYSTEM" == "upstart" ]; then
        service nginx restart
    else
        systemctl restart nginx
    fi
}

configure_init() {
    if [ "$INITSYSTEM" == "upstart" ]; then
        configure_upstart
    else
        configure_systemd
    fi
}

configure_upstart() {
    cat > "/etc/init/meteor_$USER.conf" << EOF
description "$USER - Meteor.js (NodeJS) application"
start on started mongodb and runlevel [2345]
stop on shutdown
respawn
respawn limit 10 5
setuid $USER
setgid $USER
script
    export PATH=/opt/local/bin:/opt/local/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    export NODE_PATH=/usr/lib/nodejs:/usr/lib/node_modules:/usr/share/javascript
    export PWD=$FULLPATH
    export HOME=$FULLPATH
    export BIND_IP=127.0.0.1
    export PORT=$PORT
    export HTTP_FORWARDED_COUNT=1
    export MONGO_URL=mongodb://localhost:27017/$USER
    export ROOT_URL=$SERVERNAME
    export METEOR_SETTINGS='{}'
    # export MAIL_URL=smtp://postmaster@mymetorapp.net:password123@smtp.mailgun.org
    exec $NODE $FULLPATH/bundle/main.js >> $FULLPATH/meteor.log
end script
EOF
}

configure_systemd() {
    cat > "/etc/systemd/system/meteor_$USER.service" << EOF
[Unit]
Description=$USER - Meteor.js (NodeJS) application

[Service]
Environment=PATH=/opt/local/bin:/opt/local/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
Environment=NODE_PATH=/usr/lib/nodejs:/usr/lib/node_modules:/usr/share/javascript
Environment=PWD=$FULLPATH
Environment=HOME=$FULLPATH
Environment=BIND_IP=127.0.0.1
Environment=PORT=$PORT
Environment=HTTP_FORWARDED_COUNT=1
Environment=MONGO_URL=mongodb://localhost:27017/$USER
Environment=ROOT_URL=$SERVERNAME
Environment=METEOR_SETTINGS='{}'
#Environment=MAIL_URL=smtp://postmaster@mymetorapp.net:password123@smtp.mailgun.org
ExecStart=$NODE $FULLPATH/bundle/main.js >> $FULLPATH/meteor.log
Restart=always

[Install]
WantedBy=multi-user.target
EOF
    systemctl daemon-reload
    systemctl enable meteor_$USER.service
}

restart_script() {
    if [ "$INITSYSTEM" == "upstart" ]; then
        stop meteor_$USER
        start meteor_$USER
    else
        systemctl stop meteor_$USER.service
        systemctl start meteor_$USER.service
    fi
}

# Verifições básicas

if [[ $EUID -ne 0 ]]; then
   echo "ERROR: This script must be run as root! > sudo $0" 1>&2
   exit 1
fi

if ! id "$USER" >/dev/null 2>&1; then
   echo "ERROR: The user '$USER' does not exist! > sudo adduser --disabled-login $USER" 1>&2
   exit 1
fi

if [[ -z $NODE ]]; then
   echo "ERROR: node not found! > sudo apt-get install nodejs" 1>&2
   exit 1
fi

if [[ -z $METEOR ]]; then
   echo "ERROR: meteor not found! > sudo curl https://install.meteor.com/ | sh" 1>&2
   exit 1
fi

if [[ -z $GIT ]]; then
   echo "ERROR: git not found! > sudo apt-get install git" 1>&2
   exit 1
fi

if [[ -z $MONGO ]]; then
   echo "WARNING: mongo not found! > sudo apt-get install mongodb" 1>&2
fi

if [ ! -f $GITSSHKEY ]; then
    echo "ERROR: SSH key not found!" 1>&2
    exit 1
fi

ssh-agent bash -c "ssh-add $GITSSHKEY; $GIT ls-remote $GITREPOSITORY $FULLPATH/git" >/dev/null 2>&1
if [ $? != 0 ]; then
   echo "ERROR: git repository not found!" 1>&2
   exit
fi

if [[ -z $NGINX ]]; then
   echo "ERROR: nginx not found! > sudo apt-get install nginx" 1>&2
   exit 1
fi

if [[ -z $NPM ]]; then
   echo "ERROR: npm not found! > sudo apt-get install npm" 1>&2
   exit 1
fi

if [ ! -f "/etc/nginx/sites-enabled/$SERVERNAME.conf" ]; then
    echo "The nginx configuration file was not found."
    while true; do
        read -p "Would you like to create one now? [Y]es or [N]o: " yn
        case $yn in
            [YySs]* ) configure_nginx; break;;
            [Nn]* ) echo "WARNING: nginx not configured!";;
            * ) echo "Type Y or N.";;
        esac
    done
fi

if [ "$INITSYSTEM" == "upstart" ]; then
    if [ ! -f "/etc/init/meteor_$USER.conf" ]; then
        configure_upstart
    fi
else
    SYSTEMCTL=$(which systemctl)
    if [[ -z $SYSTEMCTL ]]; then
       echo "ERROR: systemctl not found! > sudo apt-get install systemd" 1>&2
       exit 1
    fi

    if [ ! -f "/etc/systemd/system/meteor_$USER.service" ]; then
        configure_systemd
    fi
fi


# Inicia a rotina que realiza o deploy

#clona o repositorio git
if [ ! -d "$FULLPATH/git" ]; then
    ssh-agent bash -c "ssh-add $GITSSHKEY; $GIT clone $GITREPOSITORY $FULLPATH/git"
fi

if [ ! -f "$FULLPATH/git/.git/config" ]; then
    rm -rf "$FULLPATH/git"
    ssh-agent bash -c "ssh-add $GITSSHKEY; $GIT clone $GITREPOSITORY $FULLPATH/git"
fi

grep "$GITREPOSITORY" "$FULLPATH/git/.git/config" >/dev/null 2>&1
if [ $? != 0 ]; then
   rm -rf "$FULLPATH/git"
   ssh-agent bash -c "ssh-add $GITSSHKEY; $GIT clone $GITREPOSITORY $FULLPATH/git"
fi

cd "$FULLPATH/git"
ssh-agent bash -c "ssh-add $GITSSHKEY; $GIT fetch origin && $GIT reset --hard origin/$GITBRANCH && $GIT checkout $GITBRANCH"

chown $USER:$USER "$FULLPATH/git"

#exec sudo -u $USER /bin/sh - << eof
su $USER << eof

    cd "$FULLPATH/git"

    #gera o pacote da aplicação
    $METEOR remove-platform android
    $METEOR build "$FULLPATH"

    if [ ! -f "$FULLPATH/git.tar.gz" ]; then
       exit 1
    fi

    #NOTA: caso ocorra o erro "FATAL ERROR: Evacuation Allocation failed - process out of memory"
    #é pq não existe Swap (https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04)

    #remove o diretório existente da aplicação
    rm -rf "$FULLPATH/bundle"

    #extrai o pacote gerado
    tar -zxf "$FULLPATH/git.tar.gz" -C "$FULLPATH/"

    #executa o instalador
    cd "$FULLPATH/bundle/programs/server"
    $NPM install --production
    #npm prune --production

eof

if [ $? != 0 ]; then
   echo "ERROR: meteor build failed!" 1>&2
   exit $?
fi

#corrige o usuário dos arquivos
chown -R $USER:$USER "$FULLPATH"

#reinicia o script
restart_script

#remove o repositório git clonado
#rm -rf "$FULLPATH/git"
